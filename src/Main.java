import entity.*;
import function.SweetsAction;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.Scanner;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        Comparator<Sweets> comparator = new SweetsSugarContentComparator().thenComparing(new SweetsWeightComparator()).
                thenComparing(new SweetsPriceComparator()).thenComparing(new SweetsNameComparator());
        TreeSet<Sweets> set = new TreeSet<Sweets>(comparator);
        EnumSet<SweetsType> enumSet = EnumSet.allOf(SweetsType.class);
        Scanner input = new Scanner(System.in);
        String inputSweet = "";
        int countOfSweets = 0;
        while (true){
            if (countOfSweets == 9){
                System.out.println("Sweets are over.");
                break;
            }
            System.out.print("Which sweets would you like to add in the present? Write name of the sweet.\n" +
                    "Enter 'Exit' to stop the process:\n");
            for (SweetsType sweetsType : enumSet){
                System.out.println(sweetsType);
            }
            inputSweet = input.nextLine();
            inputSweet = inputSweet.toUpperCase().trim();
            if (inputSweet.equals("EXIT")){
                break;
            }
            if (enumSet.contains(SweetsType.valueOf(inputSweet))){
                enumSet.remove(SweetsType.valueOf(inputSweet));
                set.add(new Sweets(SweetsType.valueOf(inputSweet)));
                System.out.println(inputSweet + " was added!");
                countOfSweets++;
            }
            else if (set.contains(new Sweets(SweetsType.valueOf(inputSweet)))){
                System.out.println(inputSweet + " was already added!");
            }
            else {
                System.out.println("Invalid command! Repeat.");
            }
        }
        System.out.println("Your present looks like:");
        for (Sweets sweets : set) {
            System.out.println(sweets);
        }
        SweetsAction sweetsAction = new SweetsAction();
        double lowerbound = 0, upperbound = 0;
        while (true) {
            System.out.print("Please enter sugar content range percent values from 0 % to 100 % :\nLowerbound - ");
            try {
            lowerbound = Double.parseDouble(input.next());
            if (!sweetsAction.isValidNumber(lowerbound)){
                throw new IllegalArgumentException();
            }
            break;
            }
            catch (IllegalArgumentException ignore){
                System.out.println("Invalid input");
            }
        }
        while (true) {
            System.out.print("Upperbound - ");
            try {
                upperbound = Double.parseDouble(input.next());
                if (!sweetsAction.isValidNumber(upperbound) || upperbound < lowerbound){
                    throw new IllegalArgumentException();
                }
                break;
            }
            catch (IllegalArgumentException ignore){
                System.out.println("Invalid input");
            }
        }
            System.out.println("The result: ");
            sweetsAction.findSweetsInRange(lowerbound, upperbound, set);
    }
}
