package entity;

import java.util.Comparator;

public class SweetsSugarContentComparator  implements Comparator<Sweets> {
    @Override
    public int compare(Sweets o1, Sweets o2) {
        if(o1.getSweetsType().getSugarContent() > o2.getSweetsType().getSugarContent()) {
            return 1;
        }
        else if(o1.getSweetsType().getSugarContent() < o2.getSweetsType().getSugarContent()) {
            return -1;
        }
        return 0;
    }
}

