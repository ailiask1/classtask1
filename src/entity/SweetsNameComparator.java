package entity;

import java.util.Comparator;

public class SweetsNameComparator implements Comparator<Sweets> {
    @Override
    public int compare(Sweets o1, Sweets o2) {
        return o1.getSweetsType().name().compareTo(o2.getSweetsType().name());
    }
}

