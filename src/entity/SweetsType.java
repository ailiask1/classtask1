package entity;

public enum SweetsType {
    CAKE(27, 250, 0.29), CANDY(52, 200, 4.74), CHOCOLATE(48, 120, 1.32), COOKIE(15, 200, 2.5),
    JELLY(13, 150, 1.42), LOLLIPOP(50, 40, 1.05), MARMALADE(60, 100, 2.11), SLIDER(30, 120, 1.04),
    ZEPHYR(60, 90, 2.27);
    private double sugarContent;
    private double weight;
    private double price;

    SweetsType(double sugarContent, double weight, double price) {
        this.sugarContent = sugarContent;
        this.weight = weight;
        this.price = price;
    }

    public double getSugarContent() {
        return sugarContent;
    }

    public double getWeight() {
        return weight;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Name - " + name() +
                ", Sugar Content - " + getSugarContent() +
                " %, Weight - " + getWeight() +
                " g, Price - " + getPrice() +
                " $";
    }
}
