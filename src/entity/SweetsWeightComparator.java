package entity;

import java.util.Comparator;

public class SweetsWeightComparator  implements Comparator<Sweets> {
    @Override
    public int compare(Sweets o1, Sweets o2) {
        if (o1.getSweetsType().getWeight() > o2.getSweetsType().getWeight()) {
            return 1;
        } else if (o1.getSweetsType().getWeight() < o2.getSweetsType().getWeight()) {
            return -1;
        }
        return 0;
    }
}

