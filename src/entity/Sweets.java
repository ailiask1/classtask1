package entity;

public class Sweets{
    private SweetsType sweetsType;

    public Sweets(SweetsType sweetsType) {
        this.sweetsType = sweetsType;
    }

    public SweetsType getSweetsType() {
        return sweetsType;
    }

    @Override
    public String toString() {
        return "Name - " + sweetsType.name() +
                ", Sugar Content - " + sweetsType.getSugarContent() +
                " %, Weight - " + sweetsType.getWeight() +
                " g, Price - " + sweetsType.getPrice() +
                " $";
    }
}
