package entity;

import java.util.Comparator;

public class SweetsPriceComparator  implements Comparator<Sweets> {
    @Override
    public int compare(Sweets o1, Sweets o2) {
        if (o1.getSweetsType().getPrice() > o2.getSweetsType().getPrice()) {
            return 1;
        } else if (o1.getSweetsType().getPrice() < o2.getSweetsType().getPrice()) {
            return -1;
        }
        return 0;
    }
}
