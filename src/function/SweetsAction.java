package function;

import entity.Sweets;
import java.util.Iterator;
import java.util.TreeSet;

public class SweetsAction {
    public void findSweetsInRange(double lowerBound, double upperBound, TreeSet<Sweets> sweets){
        int count = 0;
        Iterator<Sweets> iterator = sweets.iterator();
        while (iterator.hasNext()){
            Sweets sweet = iterator.next();
            if (sweet.getSweetsType().getSugarContent() >= lowerBound && sweet.getSweetsType().getSugarContent() <= upperBound){
                count++;
                System.out.println(sweet.toString());
            }
        }
        if (count == 0){
            System.out.println("There is no sweet which pleasures the sugar content range.");
        }
    }

    public boolean isValidNumber(double bound){
        if (bound >= 0 && bound <= 100){
            return true;
        }
        return false;
    }
}
